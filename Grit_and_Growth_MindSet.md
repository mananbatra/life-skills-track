# Grit and Growth Mindset

## 1. Grit
[Grit Video](https://www.youtube.com/watch?v=H14bBuluwB8)

#### Summary of the Video

Grit means having a strong passion and determination to achieve long-term goals. It's about being able to endure challenges and setbacks along the way. Grit involves consistently committing to your future and putting in hard work every day, not just for a short time but for many years. It's like approaching life as a marathon, where you keep going steadily instead of rushing through it quickly. Grit is the inner strength that keeps you going when things get tough, pushing through obstacles and staying focused on your dreams. It's about having the resilience to overcome difficulties and the perseverance to keep going, no matter how long it takes.


#### What are your key takeaways from the video to take action on?

Having a growth attitude can help you build grit. Learning ability is not fixed and can be improved with our efforts. We are far more inclined to persevere when we understand that failure is not a permanent condition when we grasp how our brain functions and grows in response to adversity.  

## 2. Introduction to Growth Mindset

[Introduction to Growth Mindset Video](https://www.youtube.com/watch?v=75GFzikmRY0)

#### Summary of the Video

1. The growth mindset is a conviction in one's own potential to learn and grow, and it is transforming and improving people's learning.
2. People's perspectives are critical to their success.
3. People with a fixed mindset believe that you do not have control over your abilities; they feel that skills are innate and that you cannot learn or grow, whereas people with a growth mindset believe the reverse.
4.  The cornerstone for learning is a growth mentality.
5. A growth mentality is built on two pillars: belief and attention.
6. Hard work, challenges, mistakes, and criticism are the keys to success. All of these keys are byproducts of belief and concentration.

A growth mindset is the foundation of learning.

#### What are your key takeaways from the video to take action on?

Having a growth mindset is crucial and holds great power when it comes to learning. It forms the very basis of our ability to acquire new knowledge and skills. A growth mindset thrives when we persist in our efforts and embrace the opportunity to learn from our mistakes. By cultivating a growth mindset, we open ourselves up to achieving a multitude of goals throughout our lives.


## 3. Understanding Internal Locus of Control

[How to stay motivated Video - The Locus Rule ](https://www.youtube.com/watch?v=8ZhoeSaPF-k)

#### What is the Internal Locus of Control? What is the key point in the video?

The locus of control is the amount of control you believe you have over your own life. An internal locus of control indicates that you believe your own actions and efforts contribute to your success in a specific area. It is the conviction that your hard effort and determination will result in positive results over time. Having an internal locus of control is vital for long-term motivation because it helps you stay focused and driven, knowing that your efforts will be noticed. On the other side, blaming external events beyond your control can demotivate you from continuing to work on a task or pursue a goal.

## 4. How to build a Growth Mindset

[How to build a Growth Mindset Video](https://www.youtube.com/watch?v=9DVdclX6NzY)

#### Summary

Having faith in your talents can ultimately help you reach the goals you set for yourself. A negative or stagnant mindset causes you to believe you're not good enough for anything, stifling your growth. In any work, you should be eager to learn and develop. If you're new to something, don't be afraid to be bad at it. Seeking to progress implies being open to difficulties, disappointments, and setbacks. Respect the struggle and the difficulties. Consider these difficulties to be chances to learn from and develop mental tools to help you deal with similar problems in the future. 

#### What are your key takeaways from the video to take action on?

1. Beliving in my ablitiy whatever may be the situation.
2. Maintain checklist of my short and long term goals.
3. Question my assumptions.

## 5. Mindset - A MountBlue Warrior Reference Manual

[Link to Google Docs - Mindset](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk)

#### What are one or more points that you want to take action on from the manual?

1. I know more efforts lead to better understanding.

2. I will not leave my code unfinished till I complete the following checklist:
Make it work.
Make it readable.
Make it modular.
Make it efficient.

