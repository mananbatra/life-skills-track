## Q1. What are the activities you do that make you relax - Calm quadrant?

1. Engaging in meditation or deep breathing exercises.
2. Going for a leisurely walk in nature.
3. Spending quality time with loved ones and pets.
4. Engaging in hobbies such as painting, reading, or gardening.

## Q2. When do you find getting into the Stress quadrant?

1. When facing tight deadlines and a heavy workload.
2. During conflicts or disagreements with others.
3. When dealing with unexpected or challenging situations.
4. When experiencing financial difficulties or uncertainty.

## Q3. How do you understand if you are in the Excitement quadrant?

1. Feeling a sense of joy and enthusiasm about upcoming events or opportunities.
2. When achieving a significant personal or professional goal.
3. Engaging in thrilling activities such as adventure sports or travel.
4. Being part of a lively and positive social gathering.


## Q4. Paraphrase the Sleep is your Superpower video in detail.

1. Prioritizing and ensuring a consistent 8 hours of sleep daily is essential for overall well-being.
2. Inadequate sleep can accelerate aging, making a person seem older than their actual age.
3. People who regularly get 7-8 hours of sleep tend to have a brain that operates 40% more efficiently than those who sleep only 4-5 hours.
4. Research suggests that insufficient sleep can negatively impact the immune system, making one more susceptible to illnesses.
5. Lack of sleep has also been linked to an increased risk of developing certain types of cancer.


## Q5. What are some ideas that you can implement to sleep better?


1. Establishing a bedtime routine and sticking to a consistent sleep schedule.
2. Creating a comfortable sleep environment, including a supportive mattress and suitable room temperature.
3. Limiting screen time and exposure to electronic devices before bedtime.
4. Engaging in relaxation techniques, such as reading or listening to soothing music, to unwind before sleep.


## Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* The brain's prefrontal cortex and hippocampus are two crucial areas responsible for decision-making, focus, attention, personality, and long-term memory.
* Regular exercise stimulates the production of new brain cells, leading to an increase in hippocampus volume and improved long-term memory function.
* Engaging in physical activity has brain-changing effects that enhance memory, cognitive abilities, and overall mental well-being.
* Consistent exercise supports mental clarity and focus, enabling individuals to sustain attention on tasks for extended periods.
* Incorporating exercise into one's daily routine promotes not only physical health but also cognitive fitness, resulting in a sharper and more agile mind.

## Q7. What are some steps you can take to exercise more?

* Setting achievable fitness goals and creating a workout plan to track progress.
* Incorporating short bursts of physical activity throughout the day, such as stretching or short walks.
* Exploring various forms of exercise to find activities that are enjoyable and suit personal preferences.
* Enlisting a workout buddy or joining a fitness class to stay motivated and accountable.
* Integrating exercise into daily routines, such as using stairs instead of elevators or biking to nearby destinations.