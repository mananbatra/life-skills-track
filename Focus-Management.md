## 1. What is Deep Work

Video: 5:08 minutes - [https://www.youtube.com/watch?v=b6xQpoVgN68](https://www.youtube.com/watch?v=b6xQpoVgN68)

### What is Deep Work?

Deep work is the ability to concentrate deeply on a difficult subject for extended periods of time without being distracted. It entails working with complete focus and involvement, allowing individuals to generate high-quality work and accomplish meaningful outcomes. Deep work necessitates the creation of an environment free of distractions, such as social media or phone notifications, as well as the allocation of uninterrupted time to the subject at hand.

### Paraphrase all the ideas in the above videos

- The first video emphasises the value of deep work and contends that 45 to an hour is the ideal amount of time for such work. In order to maximise productivity, one should minimise interruptions and distractions during this period.

- The advantages of deadlines are shown in the second video. In order to stay engaged to meaningful work without continually wondering whether to take breaks or delay, deadlines might serve as motivational cues.

- The third video offers a synopsis of the book "Deep Work" and makes recommendations for ways to apply deep work to everyday activities. These tactics include planning breaks, developing a deep work habit, and getting enough sleep for better cognitive function.

### How can you implement the principles in your day-to-day life?

Consider the following methods to incorporate deep work ideas into your daily life:

1. Devote at least one hour every day to intense work, ensuring that you focus on a difficult assignment without interruptions.

2. To minimise continual interruptions during intensive work sessions, schedule particular times for dealing with possible distractions, such as checking social media or emails.

3. Make deep work a habit of doing it on a regular basis. Consistency is essential for increasing attention and productivity.

4. Prioritise appropriate sleep to ensure that your mind is well-rested and capable of engaging in deep, concentrated work.

### My key takeaways from the video

Video: 13:50 minutes - [https://www.youtube.com/watch?v=3E7hkPZ-HTk](https://www.youtube.com/watch?v=3E7hkPZ-HTk)

Some major points from the film regarding the hazards of social media include:

- Although social media is not a basic technology, it has a substantial influence on people's productivity, frequently leading to diversions and a lack of attention on essential activities.

- Social media platforms are meant to catch and retain users' attention, therefore keeping them interested and potentially hooked to their services.

- Excessive social media use can have a severe impact on mental health, resulting in concerns such as anxiety, sadness, and diminished general well-being.

- These lessons emphasise the necessity of being careful of how we use social media and setting limits to maintain our concentration, productivity, and mental health.

