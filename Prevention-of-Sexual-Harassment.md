# Prevention of Sexual Harassment #

Sexual harassment refers to unwelcome verbal, visual, or physical conduct of a sexual nature that creates a hostile or offensive environment.


## What kinds of behavior cause sexual harassment? ##

1. **Unwelcome verbal conduct of a sexual nature, including:**

    - Comments about clothing, body
    - Sexual or gender-based jokes or remarks
    - Requests for sexual favors or repeatedly asking a person out
    - Sexual innuendos, threats, spreading rumors
    - Using foul and obscene language

2. **Unwelcome visual conduct of a sexual nature, including:**

    - Posters, drawings, pictures of a sexual nature
    - Sexual screensavers, cartoons
    - Emails or texts of a sexual nature

3. **Unwelcome physical conduct of a sexual nature, including:**

    - Sexual assault
    - Impeding or blocking movement
    - Inappropriate touching (kissing, hugging, padding, stroking, rubbing)
    - Sexual gesturing
    - Leering or staring



## What would you do in case you face or witness any incident or repeated incidents of such behavior? ##

If I were to face or witness any incident or repeated incidents of sexual harassment, I would take immediate action to address the situation and ensure a safe environment for myself and others. Here are the steps I would follow:

- Evaluate the issue and make sure about personal safety.
- Keep a record of the incidents' specifics (dates, times, locations, and parties involved).
- If it's safe to do so, confront the harasser immediately and convey your discomfort or disapproval.
- Inform the proper party within the company about the incidences (supervisor, HR, designated contact).
- Provide written documentation and actively participate in any internal inquiries or legal actions.
- If necessary, seek out emotional support from friends, family, or therapists.
- If the organisation doesn't act appropriately, think about bringing incidents to the attention of outside authorities or seeking legal counsel.
