# Listening and Assertive Communication #

## 1. Active Listening ##
'''
  "Active communication is the art of truly listening and engaging, fostering understanding and connection."
'''

#### What are the steps/strategies to do Active Listening? ####

1. Maintain your focus:

    - Maintain direct eye contact with the speaker.
    - Avoid getting distracted by any external factors.

2. Demonstrate active listening:

    - Nod your head to indicate understanding and engagement.
    - Use facial expressions to show your interest and comprehension.
    - Display positive body language, such as leaning forward and maintaining an open posture.
    - Offer a friendly smile to encourage the speaker.

3. Utilize door-openers:

    - Engage the speaker further by using phrases like "Tell me more" or "That sounds interesting."

4. Avoid interrupting:

    - Refrain from interrupting the speaker and allow them to complete their thoughts.

5. Provide feedback:

    - Reflect back on what the speaker said using phrases like "What I'm hearing is..." or "Sounds like you are saying...," which demonstrate that you have been actively listening and understood their message.

## 2. Reflective Listening ##
'''
 "Reflective listening is the art of attentively understanding and echoing the thoughts, feelings, and messages of the speaker."
''' 
#### According to Fisher's model, what are the key points of Reflective Listening? ####

1. Concentrate on the speaker: Pay complete attention to the person speaking and their message.

2. Show comprehension: Show comprehension by reflecting back the speaker's words and thoughts.

3. To reaffirm your knowledge and stimulate more discussion, use paraphrasing: summarise or rephrase what the speaker stated.

4. Empathise with the speaker: Without passing judgement, try to comprehend and accept the speaker's emotions and experiences.

5. Allow the speaker to fully express themselves before providing your own opinions or replies.

7. Validate the speaker's point of view: Recognise the speaker's point of view, even if you disagree with it.

8. Maintain an open, non-defensive posture: To foster open conversation, create an atmosphere of trust and acceptance.

## 3. Reflection ##

#### What are the obstacles in your listening process? ####

- **Distraction**: Nowadays, primary barriers to listening include cell phones, television, and other electronic devices.
- **Preaccupation** is the process of thinking about other things while listening to others.
- **Detailing**: Being overly concerned with the details.
- **Topic**: It appears tough to concentrate on a single topic if that topic is not of interest to you.
- **The Speaker**: If the speaker is a soft talker or a bore, this could be a listening barrier.

#### What can you do to improve your listening? ####

The following are some areas where I can improve by listening to others:

- For a while, anything that may distract you from listening will be turned off.
- I'll put myself in focused mode while listening.
- I will make certain that all of the points are clear and understood.
- While listening, I will not interrupt the speaker; instead, I will wait for them to finish before beginning to speak.
- My body language will show that I am paying attention to them.

## 4. Types of Communication ##

- Passive Communication
- Aggressive Communication
- Passive Aggressive Communication
- Aggressive Communication

#### When do you switch to Passive communication style in your day to day life? ####

1. When I am forced to work on something I don't want to do.
2. When someone don't treat me well.
3. When I do something wrong.

#### When do you switch into Aggressive communication styles in your day to day life? 

1. Someone trying to hurt my family and friends.
2. When I see someone passing racial comments to anyone.
3. Not giving priority to elder and disabled people.

#### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

1. When one don't listen, depsite saying politely often.
2. When someone try to invade my privacy.


#### How can you make your communication assertive? 

1. Use 'I' statements
2. Practice saying no
3. Use body language
4. Keep emotions in check i.e. don't become aggressive or burst out.